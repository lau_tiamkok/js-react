// Properties:
// Everything in this.props is passed down to you from the parent.
// That includes the values that were declared in the element attributes, just
// like in regular HTML where you declare attributes like class or href.
// However, in React you can set a JSON blob in the attributes instead of having
// to declare an attribute for each property:
var data = {
  title: 'Professional Node.js',
  author: 'Pedro Teixeira'
};

var Book = React.createClass({
  render: function() {
    return (
      <tr>
        <td>{this.props.data.title}</td>
        <td>{this.props.data.author}</td>
      </tr>
    );
  }
});

ReactDOM.render(<Book data={data}/>, document.getElementById('container'));
