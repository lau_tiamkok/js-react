// ReactJS fetches data from server using 'url' or 'source' attribute
// The React docs recommend performing a GET request in componentDidMount() and storing the data in state.
var UserGist = React.createClass({

    // First, initialize the data variables:
    getInitialState: function() {
        return {
            username: '',
            lastGistUrl: ''
        };
    },
    // Then fetch the data using $.get():
    componentDidMount: function() {
    this.serverRequest = $.get(this.props.source, function (result) {
        if (this.isMounted()) {
            var lastGist = result[0];
            this.setState({
                username: lastGist.owner.login,
                lastGistUrl: lastGist.html_url
            });
        }
    }.bind(this));
    },

    componentWillUnmount: function() {
        this.serverRequest.abort();
    },

    render: function() {
        return (
            <div>
                {this.state.username}'s last gist is <a href={this.state.lastGistUrl}>here</a>.
            </div>
        );
    }
});

ReactDOM.render(
    <UserGist source="https://api.github.com/users/octocat/gists" />,
    document.getElementById('example') //mountNode
);
