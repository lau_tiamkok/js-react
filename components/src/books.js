// Components:
// Here we follow the exact same logic as we did in our Hello World example, but
// this time we composed an element of another element. We even passed a
// property from the parent element to the child element.

// Components are a very useful way to compose and reuse views (and logic).
//
// ref: http://blog.yld.io/2015/06/10/getting-started-with-react-and-node-js/#.Vwd1s9-6zVM
var Books = React.createClass({
  render: function() {
    return (
      <table>
        <thead>
          <tr>
            <th>Title</th>
          </tr>
        </thead>
        <tbody>
          <Book title='Professional Node.js'></Book>
          <Book title='Node.js Patterns'></Book>
        </tbody>
      </table>
    );
  }
});

var Book = React.createClass({
  render: function() {
    return (
      <tr>
        <td>{this.props.title}</td>
      </tr>
    );
  }
});

ReactDOM.render(<Books />, document.getElementById('container'));
