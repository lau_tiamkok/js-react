// State:

// State is simply data that can change. This data is called state, because it
// represents the “state” of your application. If this data changes, your
// application will likely look different, hence being in a different “state”.
// State is usually things like a list of todos, or an enabled/disabled button.

// The root component will serve as the holder of state. State changes start at
// the root component, which is then responsible for updating the properties of
// its child components.

// Component properties are immutable, meaning they can’t be modified. So, if
// you can’t modify the properties, how do they ever change? You re-render, the
// application by calling setState(), like seen in componentDidMount().

//The setState() function is special because every time it is called, it will
//attempt to re-render the entire app. This means that if a child property is
//different from the last state, it will be re-rendered with the new value.

// https://www.firebase.com/blog/2016-01-20-tutorial-firebase-react-native.html

// Events:

// Now we need to add a read checkbox to each book that mutates its
// state. For that, we need to register a listener for the checked event.

// Registering an event listener is as simple as passing a function to the
// attribute in the HTML. You can see all the supported events in the official
// documentation.

// Notice, though, that when we click the checkbox the state doesn't change.
// This is because the variable state is not changed, therefore the view doesn't
// change.

// State: In order for each Book to have a state that we can mutate and see the
// change reflected in the view, we need to add a getInitialState function that
// defines the initial state of the component and assigns it to this.state.

// Now, if we try again we should see the checkbox changing, right? Not really,
// although we can see in the logs that this.state.read is getting changed every
// time we click in the checkbox.

// What is missing then? Changing the value of the state is not enough, we need
// to trigger the UI updates. To do that we can call setState which will merge
// the current state with the next state being applied to the view.

var Books = React.createClass({
  render: function() {
    return (
      <table>
        <thead>
          <tr>
            <th>Title</th>
            <th>Read</th>
          </tr>
        </thead>
        <tbody>
          <Book title='Professional Node.js'></Book>
          <Book title='Node.js Patterns'></Book>
        </tbody>
      </table>
    );
  }
});

var Book = React.createClass({
  getInitialState: function() {
    return {
      read: false
    };
  },
  handleChange: function(ev) {
    this.setState({
      read: !this.state.read
    });
  },
  render: function() {
    return (
      <tr>
        <td>{this.props.title}</td>
        <td><input type='checkbox' checked={this.state.read} onChange={this.handleChange} /></td>
      </tr>
    );
  }
});

ReactDOM.render(<Books />, document.getElementById('container'));
